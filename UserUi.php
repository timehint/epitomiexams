<?php
function EExams_createQuixUI($args)
{
    if(!current_user_can( 'can_create_quizes' ) && !current_user_can( 'manage_options' ) ) { 
        wp_die( "Sorry, you don't have permission to view this page.");
    }
    global $post;
    ob_start();
    if(isset($_GET['ids']))
    {
        wp_enqueue_script("jquery");
        wp_enqueue_script('EExams_mathjax', "https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=default");
        wp_enqueue_script('EExams_pdf', "https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.debug.js");
        wp_enqueue_script('EExams_CustomFontpdf', plugins_url('js/Roboto-normal.js?verison=1', __FILE__));
        //wp_enqueue_script('EExams_VFS', "https://cdn.rawgit.com/sphilee/jsPDF-CustomFonts-support/bcc544cd/dist/default_vfs.js");
        wp_enqueue_script('EExams_canvas', plugins_url('js/html2canvas.js', __FILE__));
        wp_enqueue_script('EExams_canvas2image', "https://cdn.jsdelivr.net/npm/canvas2image@1.0.5/canvas2image.min.js");
        wp_enqueue_script('EExams_generatePDF', plugins_url('js/generatePDF.js?verison=2.3', __FILE__), array("jquery"), false, true); 
        wp_enqueue_script('pdfobject', "https://cdnjs.cloudflare.com/ajax/libs/pdfobject/2.1.1/pdfobject.min.js");
        echo '<script type="text/x-mathjax-config">MathJax.Hub.Config({ 
                    jax: ["input/TeX", "output/SVG"],
                    extensions: ["tex2jax.js", "MathMenu.js", "MathZoom.js"],
                    showMathMenu: false,
                    showProcessingMessages: false,
                    messageStyle: "none",
                    SVG: {
                    useGlobalCache: false
                    },
                    TeX: {
                    extensions: ["AMSmath.js", "AMSsymbols.js", "autoload-all.js"]
                    }
                });</script>
        ';
            $ids = json_decode(stripslashes($_GET['ids']));
            $posts = array();
            foreach ($ids as $id) {
                $post = get_post($id);
                $question = $post->post_content;
                $question = apply_filters('the_content', $question);
                $question = str_replace(']]>', ']]&gt;', $question);
                $answers = get_post_meta($post->ID, "_answers", true);
                $type = get_post_meta($post->ID, "_type", true);
                $correct_answers = get_post_meta($post->ID, "_correct_answer", true);
                $temp = array(
                    "post" => $post,
                    "question" => $question,
                    "answers" => $answers,
                    "type" => $type,
                    "correct_answer" => $correct_answers,
                    "points" => get_post_meta($post->ID, "_points", true )
                );
                array_push($posts, $temp);
            }

            $pdf_settings = [
                "EExams_pdf_title_enabled" => get_option("EExams_pdf_title_enabled"),
                "EExams_pdf_title_font_size" => get_option("EExams_pdf_title_font_size"),
                "EExams_pdf_title_color" => get_option("EExams_pdf_title_color"),

                "EExams_pdf_subtitle_enabled" => get_option("EExams_pdf_subtitle_enabled"),
                "EExams_pdf_subtitle_font_size" => get_option("EExams_pdf_subtitle_font_size"),
                "EExams_pdf_subtitle_color" => get_option("EExams_pdf_subtitle_color"),

                "EExams_pdf_question_title_enabled" => get_option("EExams_pdf_question_title_enabled"),
                "EExams_pdf_question_title_font_size" => get_option("EExams_pdf_question_title_font_size"),
                "EExams_pdf_question_title_color" => get_option("EExams_pdf_question_title_color"),
                "EExams_pdf_questions_spacer" => get_option("EExams_pdf_questions_spacer"),
                
                "EExams_pdf_points_enabled" => get_option("EExams_pdf_points_enabled"),
                "EExams_pdf_points_font_size" => get_option("EExams_pdf_points_font_size"),
                "EExams_pdf_points_color" => get_option("EExams_pdf_points_color"),
                "EExams_pdf_points_text" => get_option("EExams_pdf_points_text"),
                
                "EExams_pdf_footnote_enabled" => get_option("EExams_pdf_footnote_enabled"),
                "EExams_pdf_footnote_font_size" => get_option("EExams_pdf_footnote_font_size"),
                "EExams_pdf_footnote_color" => get_option("EExams_pdf_footnote_color"),
                "EExams_pdf_footnote_text" => get_option("EExams_pdf_footnote_text"),

                "EExams_pdf_page_number_enabled" => get_option("EExams_pdf_page_number_enabled"),
                "EExams_pdf_page_number_font_size" => get_option("EExams_pdf_page_number_font_size"),
                "EExams_pdf_page_number_color" => get_option("EExams_pdf_page_number_color"),

                "EExams_pdf_box_grade_enabled" => get_option("EExams_pdf_box_grade_enabled"),

                "EExams_pdf_name_enabled" => get_option("EExams_pdf_name_enabled"),
                "EExams_pdf_name_font_size" => get_option("EExams_pdf_name_font_size"),
                "EExams_pdf_name_color" => get_option("EExams_pdf_name_color"),
                "EExams_pdf_name_text" => get_option("EExams_pdf_name_text"),
            ];
            

            wp_localize_script('EExams_generatePDF', 'EExams', array(
                "questions" => json_encode($posts),
                "iconURL" => plugin_dir_url( __FILE__ )."img/",
                "filterPage" => get_page_link( intval(get_option( 'EExams_CreateAQuizPageID' )) ),
                "settings" => $pdf_settings 
            ));
            echo '
            <div class="body-wrapper">
                <div id="scrapHtml" style="font-size:20px !important;overflow:visible !important;width:720px;background:#fff;"></div>
                <div id="scrapHtml2" style="font-size:20px !important;overflow:visible !important;width:720px;background:#fff;"></div>
                <div id="wrapper"></div>
                <div id="pdf-viewer" style="
                width: 100%;
                height: 100%;
                position: absolute;
                left: 0;
                right: 0;
                bottom: 0;
                top: 0px;
                "></div>
            </div>';
    }
    else
    {
        $posts_not_in = array();
        $edit = array();
        $answers = null;
        if(isset($_GET['edit']) && intval($_GET['edit']) !== 0)
        {
            $id = intval($_GET['edit']);
            $quizz = get_post($id);
            if($quizz)
            {
                $ids = explode(',',get_post_meta($id,"question_ids",true));
                $posts_not_in = $ids;
                $answers = get_post_meta($id,"answers",true);
                $edit['title'] = $quizz->post_title;
                $edit['subtitle'] = get_post_meta($id,"subtitle",true);
                $args = array(
                    "post_type" => "eexams_questions",
                    "post__in" => $ids,
                );
                $selected_questions = array();
                $the_query = new WP_Query( $args );
                if ( $the_query->have_posts() ) {
                    
                    while ( $the_query->have_posts() ) {
                        $the_query->the_post();
                        $rating_submissions = intval(get_post_meta( $post->ID, "_rating_submissions", true));
                        if($rating_submissions == 0)
                        {
                            $rating_submissions = 1;
                        }
                        $temp = array(
                            "post" => $post,
                            "subject" => get_the_title(intval(get_post_meta( $post->ID, "_subject", true ))),
                            "grade" => get_the_title(intval(get_post_meta( $post->ID, "_grade_id", true ))),
                            "level" => get_the_title(intval(get_post_meta( $post->ID, "_level_id", true ))),
                            "points" => get_post_meta( $post->ID, "_points", true),
                            "duration" => get_post_meta( $post->ID, "_duration", true),
                            "hours" => get_post_meta( $post->ID, "_hours", true),
                            "mins" => get_post_meta( $post->ID, "_mins", true),
                            "answers" => get_post_meta( $post->ID, "_answers",true),
                            "type" => get_post_meta( $post->ID, "_type", true),
                            "correct_answer" => get_post_meta( $post->ID, "_correct_answer", true),
                            "year" =>  get_post_meta( $post->ID, "_year", true),
                            "thumbnail" =>  get_the_post_thumbnail_url( $post->ID),
                            "rating" => floatval(intval(get_post_meta( $post->ID, "_rating", true))/intval($rating_submissions))
                        );
                        array_push($selected_questions, $temp);
                    }
                    
                    wp_reset_postdata();
                } else {
                    
                }
                $edit['selected_questions'] = $selected_questions; 
                $edit['id'] = $id;
            }
        }
        wp_enqueue_script( "jquery" );
        wp_enqueue_script( "jquery-ui-core" );
        wp_enqueue_script( "jquery-ui-sortable" );
        wp_enqueue_script( "jquery-ui-fix", plugins_url( 'js/jquery.ui.touch-punch.min.js', __FILE__ ));
        wp_enqueue_script( 'EExams_popper',"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/popper.min.js" );
        wp_enqueue_script(
        'EExams_bootstrap',"https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js" );
        wp_enqueue_script( 'EExams_swl',"https://cdn.jsdelivr.net/npm/sweetalert2@9" );
        wp_enqueue_script( 'EExams_duration',plugins_url( 'js/duration-picker.min.js', __FILE__ ) );
        wp_enqueue_script( 'EExams_readmore',plugins_url( 'js/readmore.min.js', __FILE__ ) );
        wp_enqueue_script( 'EExams_mathjax',"https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=default" );
        wp_enqueue_script( 'EExams_stary', plugins_url( 'js/jquery.barrating.min.js', __FILE__ ));
        wp_enqueue_script( 'EExams_simplemodal',"https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js" );
        wp_enqueue_script( 'EExams_pagination',plugins_url( 'js/jquery.twbsPagination.min.js?verison=1.5', __FILE__ ) );
        wp_enqueue_script( 'EExams_admincreateAquiz',plugins_url( 'js/createAquiz.js?verison=1.9', __FILE__ ) );
        // The Query 
        $args = array(
            "post_type" => 'eexams_questions',
            'posts_per_page' => intval(get_option("EExams_num_per_page")),
            'paged' => 1,
            'post_status' => 'publish',
            'post__not_in' => $posts_not_in
        );
        $posts = array();
        $the_query = new WP_Query( $args );
        // The Loop
        if ($the_query->have_posts()) {
            while ($the_query->have_posts()) {
                $the_query->the_post();
                $rating_submissions = intval(get_post_meta( $post->ID, "_rating_submissions", true));
                if($rating_submissions == 0)
                {
                    $rating_submissions = 1;
                }
                $temp = array(
                    "post" => $post,
                    "subject" => get_the_title(intval(get_post_meta( $post->ID, "_subject", true ))),
                    "grade" => get_the_title(intval(get_post_meta( $post->ID, "_grade_id", true ))),
                    "level" => get_the_title(intval(get_post_meta( $post->ID, "_level_id", true ))),
                    "points" => get_post_meta( $post->ID, "_points", true),
                    "duration" => get_post_meta( $post->ID, "_duration", true),
                    "hours" => get_post_meta( $post->ID, "_hours", true),
                    "mins" => get_post_meta( $post->ID, "_mins", true),
                    "answers" => get_post_meta( $post->ID, "_answers",true),
                    "type" => get_post_meta( $post->ID, "_type", true),
                    "correct_answer" => get_post_meta( $post->ID, "_correct_answer", true),
                    "year" =>  get_post_meta( $post->ID, "_year", true),
                    "thumbnail" =>  get_the_post_thumbnail_url( $post->ID),
                    "rating" => floatval(intval(get_post_meta( $post->ID, "_rating", true))/intval($rating_submissions))
                );
                array_push($posts, $temp);
            }
            wp_reset_postdata();
        } else {
            // no posts found
        }
        wp_localize_script( 'EExams_admincreateAquiz', 'EExams', array(
            "posts" => json_encode($posts),
            "numberOfPages" => $the_query->max_num_pages,
            'current_page' => 1,
            'ajaxurl' => admin_url( 'admin-ajax.php' ),
            'edit' => $edit,
            'iconsurl'=> plugins_url( 'img/', __FILE__ ),
            "pdfq" => get_option("EExams_PDF_q"),
            "number_of_questions_per_page" =>  intval(get_option("EExams_num_per_page")),
        ));
            //?answers=undefined&title=test&ids=[164,163,160,158,159]&i=1
        ?>
        <script type="text/x-mathjax-config">
        MathJax.Hub.Config({
            extensions: ["tex2jax.js"],
            jax: ["input/TeX", "output/HTML-CSS"],
            tex2jax: {
            inlineMath: [ [ "\\[" , "\\]" ] ],
            processEscapes: true
            },
            "HTML-CSS": { fonts: ["TeX"] }
        });
        </script>
        <div class="wrap bootstrap-wrapper">
            <h1>Create A quiz</h1>
            <div class="container-fluid items" id="filters-area">
                <div class="row filter-row">
                    <div class="col-12 col-md-3 no-gutters zero-padding"><label for="subject" class="label">Subject:</label></div>
                    <?php
                        $args = array(
                            'post_type' => 'eexams_subjects',
                        );
                        $the_query = new WP_Query($args);
                        $channels = array();
                        // The Loop
                        echo '<div class="col-12 col-md-9 no-gutters zero-padding">';
                        if ( $the_query->have_posts() ) {
                            while ( $the_query->have_posts() ) {
                                $the_query->the_post();
                                echo "<div class='subject item' id='{$post->ID}'>{$post->post_title}</div> ";
                            }
                            /* Restore original Post Data */
                            wp_reset_postdata();
                        } else {
                            echo "there is no Subjects found";
                        }
                        echo "</div>";
                    ?>
                    <input type="hidden" class="filter-input" name="subject" id="subjectInput">
                </div>
                <div class="row filter-row">
                    <div class="col-12 col-md-3 no-gutters zero-padding"><label for="grade" class="label">Grade:</label></div>
                    <?php
                        $args = array(
                            'post_type' => 'eexams_grades',
                        );
                        $the_query = new WP_Query($args);
                        $channels = array();
                        // The Loop
                        echo '<div class="col-12 col-md-9 no-gutters zero-padding">';
                        if ( $the_query->have_posts() ) {
                            while ( $the_query->have_posts() ) {
                                $the_query->the_post();
                                echo "<div class='grade item' id='{$post->ID}'>{$post->post_title}</div> ";
                            }
                            /* Restore original Post Data */
                            wp_reset_postdata();
                        } else {
                            echo "there is no Grades found";
                        }
                        echo "</div>";
                    ?>
                    <input type="hidden" class="filter-input" name="grade" id="gradeInput">
                </div>

                <div class="row filter-row">
                    <div class="col-12 col-md-3 no-gutters zero-padding"><label for="tag" class="label">Tags:</label></div>
                    <?php
                        $terms = get_terms('questions_tags');
                        echo '<div class="col-12 col-md-9 no-gutters zero-padding">';
                        //Loop through each term
                        if(count($terms) > 0)
                            foreach($terms as $term):
                                //Display tag title
                                echo "<div class='tag item' id='{$term->term_id}'>{$term->name}</div> ";
                            endforeach;//Endforeach $term
                        else
                            echo "there is no Tags assigned to questions found";
                        echo "</div>";
            
                    ?>
                    <input type="hidden" class="filter-input" name="tag" id="tagInput">
                </div>
                <div class="row filter-row less">
                    <div class="col-12 col-md-3 no-gutters zero-padding"><label for="level" class="label">Difficulty:</label></div>
                    <?php
                        $args = array(
                            'post_type' => 'eexams_diff',
                        );
                        $the_query = new WP_Query($args);
                        $channels = array();
                        // The Loop
                        echo '<div class="col-12 col-md-9 no-gutters zero-padding">';
                        if ( $the_query->have_posts() ) {
                            while ( $the_query->have_posts() ) {
                                $the_query->the_post();
                                echo "<div class='level item' id='{$post->ID}'>{$post->post_title}</div> ";
                            }
                            /* Restore original Post Data */
                            wp_reset_postdata();
                        } else {
                            echo "there is no Difficulties found";
                        }
                        echo "</div>";
                    ?>
                    <input type="hidden" class="filter-input" name="level" id="levelInput">
                </div>
                <div class="row filter-row less">
                    <div class="col-12 col-md-3 no-gutters zero-padding"><label for="points" class="label">Points:</label></div>
                    <div class="col-12 col-md-9 no-gutters zero-padding">
                        <div class='point item' data-value='<5'>
                            < 5 </div> <div class='point item' data-value='5-10'> 5 - 10
                        </div>
                        <div class='point item' data-value='10-20'> 10 - 20 </div>
                        <div class='point item' data-value='>40'> > 40 </div>
                    </div>
                    <input type="hidden" class="filter-input" name="points" id="pointInput">
                </div>
                <div class="row filter-row less">
                    <div class="col-12 col-md-3 no-gutters zero-padding"><label for="level" class="label">Duration:</label></div>
                    <div class="col-12 col-md-9 no-gutters zero-padding">
                        <div class='duration item' data-value='{"minfrom":"1","minto":"10","hourfrom":"0","hourto":"0"}'> 10 min</div>
                        <div class='duration item' data-value='{"minfrom":"10","minto":"30","hourfrom":"0","hourto":"0"}'> 10 - 30 min</div>
                        <div class='duration item' data-value='{"minfrom":"30","minto":"45","hourfrom":"0","hourto":"0"}'> 30 - 45 min</div>
                        <div class='duration item' data-value='{"minfrom":"40","minto":"60","hourfrom":"0","hourto":"0"}'> 45 - 60 min</div>
                        <div class='duration item' data-value='{"minfrom":"59","minto":"-1","hourfrom":"0","hourto":"0"}'> > 60 </div>
                    </div>
                    <input type="hidden" class="filter-input" name="duration" id="durationInput">

                </div>
                <div class="row filter-row less">
                    <div class="col-12 col-md-3 no-gutters zero-padding"><label for="level" class="label">Search:</label></div>
                    <div class="col-12 col-md-9 no-gutters zero-padding">
                        <div class="input-group searchconts">
                            <input type="text" name="search" id="search" class="filter-input form-control border-right-0"
                                placeholder="Search">
                            <span class="icon"><i class="fas fa-search"></i></span>
                            <span class="clearSearch"><i class="fas fa-times"></i></span>
                        </div>
                    </div>
                </div>
                <div class="row filter-row" style="position: relative;">
                    <div class="col-12 col-md-7">
                        <div class="row">
                            <div class="col-12 col-md-4 no-gutters zero-padding"><label for="level" class="label">Questions/Answers: </label></div>
                            <div class="col-12 col-md-8 no-gutters zero-padding">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="answers" id="answers1" value="1" 
                                    <?php $answers !== null ? checked( $answers, "1" ) : "checked" ?>>
                                    <label class="form-check-label" for="exampleRadios1">
                                        All
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" <?php checked( $answers, "0" ) ?> name="answers" id="answers2" value="0">
                                    <label class="form-check-label" for="exampleRadios2">
                                        Only Questions
                                    </label>
                                </div><br>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-5">
                        <div class="row">
                            <div class="col-12 col-md-3 no-gutters zero-padding"><label for="level" class="label">Year:</label></div>
                            <div class="col-12 col-md-5 no-gutters zero-padding">
                                <div class="form-group">
                                    <select class="form-control filter-input" id="years" name="year">
                                        <option></option>
                                        <?php 
                                            $years = get_meta_values( "_year", "eexams_questions" );
                                            foreach ($years as $year) {
                                                echo "<option value='{$year}'>{$year}</option>";
                                            } 
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="mt-1">&nbsp;</div>
                        </div>
                    </div>
                    <div id="buttons">
                        <input type="checkbox" id="sticky">
                        <span>Sticky area</span> | 
                        <span class="showLess"><a href="#" id="less">Show Less</a></span> |
                        <span class="clearFilters"><a href="#" id="clear">Clear filters</a></span>
                    </div>
                </div>
            </div>
            <div class="container-fluid items">
                <div class="row">
                    <div class="col-md-6 col-12">
                        <div class="questionsSelectedFrom"></div>
                        <nav aria-label="Page navigation example" id="pagination">
                            <ul class="pagination justify-content-end" id="paginationUl">
                                
                            </ul>
                        </nav>
                        <div class="overlay"><p>Loading</p></div>
                    </div>
                    <div class="col-md-6 col-12">
                        <div class="questionsSelectedTo">

                        </div>
                    </div>
                    <div class="row w-100">
                        <div class="col-12 w-100">
                            <button type="button" id="finish" class="btn btn-success float-right">Export PDF:</button>
                            <button type="button" id="Save" class="btn btn-success float-right">Save PDF</button>
                            <?php
                                if(isset($_GET['edit']) && intval($_GET['edit']) !== 0)
                                {
                                    ?>
                                        <button type="button" id="SaveAs" class="btn btn-success float-right">Save As PDF</button>
                                    <?php
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php
    }
    return ob_get_clean();
}

function get_meta_values( $meta_key,  $post_type = 'post' ) {

    $posts = get_posts(
        array(
            'post_type' => $post_type,
            'meta_key' => $meta_key,
            'posts_per_page' => -1,
        )
    );

    $meta_values = array();
    foreach( $posts as $post ) {
        $meta_values[] = get_post_meta( $post->ID, $meta_key, true );
    }

    return $meta_values;

}