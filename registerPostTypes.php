<?php
function EExams_postTypes()
{
    register_post_type( 'eexams_questions',
        array(
            'labels' => array(
                'name' => __( 'Questions' ),
				'singular_name' => __( 'Question' ),
				'menu_name' => __('EpitomiExams')
            ), 
            'public' => true,
            'has_archive' => true,
            'menu_position' => 5,
            'menu_icon' => 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgaGVpZ2h0PSIxNnB4IiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCAxNiAxNiIgd2lkdGg9IjE2cHgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6c2tldGNoPSJodHRwOi8vd3d3LmJvaGVtaWFuY29kaW5nLmNvbS9za2V0Y2gvbnMiIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj48dGl0bGUvPjxkZWZzLz48ZyBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiIGlkPSJJY29ucyB3aXRoIG51bWJlcnMiIHN0cm9rZT0ibm9uZSIgc3Ryb2tlLXdpZHRoPSIxIj48ZyBmaWxsPSIjMDAwMDAwIiBpZD0iR3JvdXAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC00OC4wMDAwMDAsIC00MzIuMDAwMDAwKSI+PHBhdGggZD0iTTU0Ljg3OTY4NDQsNDQzLjA1OTEgTDU0Ljg3OTY4NDQsNDQ1IEw1Ny4yMzA3NjkyLDQ0NSBMNTcuMjMwNzY5Miw0NDMuMDU5MSBaIE01Niw0NDggQzUxLjU4MTcyMTgsNDQ4IDQ4LDQ0NC40MTgyNzggNDgsNDQwIEM0OCw0MzUuNTgxNzIyIDUxLjU4MTcyMTgsNDMyIDU2LDQzMiBDNjAuNDE4Mjc4Miw0MzIgNjQsNDM1LjU4MTcyMiA2NCw0NDAgQzY0LDQ0NC40MTgyNzggNjAuNDE4Mjc4Miw0NDggNTYsNDQ4IFogTTUzLjU3MDAxOTcsNDM1LjUxMDQxIEM1Mi41ODY0NTE0LDQzNi4wNDMyMDggNTIuMDYzMTE2Nyw0MzYuOTQ3NjA5IDUyLDQzOC4yMjM2NCBMNTQuMjgwMDc4OSw0MzguMjIzNjQgQzU0LjI4MDA3ODksNDM3Ljg1MjAyNCA1NC40MDc2MjUzLDQzNy40OTM4NDUgNTQuNjYyNzIxOSw0MzcuMTQ5MDkzIEM1NC45MTc4MTg1LDQzNi44MDQzNDEgNTUuMzUwNDI0Myw0MzYuNjMxOTY4IDU1Ljk2MDU1MjMsNDM2LjYzMTk2OCBDNTYuNTgxMTk5Nyw0MzYuNjMxOTY4IDU3LjAwODU0NTgsNDM2Ljc3MTg4MSA1Ny4yNDI2MDM2LDQzNy4wNTE3MTMgQzU3LjQ3NjY2MTMsNDM3LjMzMTU0NCA1Ny41OTM2ODg0LDQzNy42NDE1OTIgNTcuNTkzNjg4NCw0MzcuOTgxODY3IEM1Ny41OTM2ODg0LDQzOC4yNzczNjkgNTcuNDg4NDk1NSw0MzguNTQ4MjQxIDU3LjI3ODEwNjUsNDM4Ljc5NDQ5MyBMNTYuODIwNTEyOCw0MzkuMTkwNzMyIEw1Ni4yNDQ1NzU5LDQzOS41NzM1MzkgQzU1LjY3NjUyNTgsNDM5Ljk0OTYzMyA1NS4zMjQxMjk1LDQ0MC4yODIwNjcgNTUuMTg3Mzc2Nyw0NDAuNTcwODUzIEM1NS4wNTA2MjM5LDQ0MC44NTk2MzkgNTQuOTY2NDY5Niw0NDEuMzgyMzU2IDU0LjkzNDkxMTIsNDQyLjEzOTAxOSBMNTcuMDY1MDg4OCw0NDIuMTM5MDE5IEM1Ny4wNzAzNDg1LDQ0MS43ODA4MzUgNTcuMTA0NTM2Miw0NDEuNTE2Njc5IDU3LjE2NzY1MjksNDQxLjM0NjU0MSBDNTcuMjY3NTg3Niw0NDEuMDc3OTAzIDU3LjQ3MDA4MzksNDQwLjg0Mjg0OSA1Ny43NzUxNDc5LDQ0MC42NDEzNyBMNTguMzM1MzA1Nyw0NDAuMjcxOTk1IEM1OC45MDMzNTU5LDQzOS44OTU5MDEgNTkuMjg3MzEsNDM5LjU4Njk3MiA1OS40ODcxNzk1LDQzOS4zNDUxOTggQzU5LjgyOTA2MTUsNDM4Ljk0NjcxOCA2MCw0MzguNDU2NDYxIDYwLDQzNy44NzQ0MTIgQzYwLDQzNi45MjUyMjUgNTkuNjA2ODQxNSw0MzYuMjA4ODY3IDU4LjgyMDUxMjgsNDM1LjcyNTMxOSBDNTguMDM0MTg0MSw0MzUuMjQxNzcxIDU3LjA0NjY4NTgsNDM1IDU1Ljg1Nzk4ODIsNDM1IEM1NC45NTMzMTU3LDQzNSA1NC4xOTA2NjcxLDQzNS4xNzAxMzUgNTMuNTcwMDE5Nyw0MzUuNTEwNDEgWiBNNTMuNTcwMDE5Nyw0MzUuNTEwNDEiIGlkPSJPdmFsIDMxOCIvPjwvZz48L2c+PC9zdmc+',
            'supports' => array(
				'editor',
				'thumbnail',
				'title',
            )
        )
	);
	register_post_type( 'eexams_grades',
        array(
            'labels' => array(
                'name' => __( 'Grades' ),
                'singular_name' => __( 'Grade' )
            ), 
            'public' => true,
            'has_archive' => true,
			'show_in_menu' => 'edit.php?post_type=eexams_questions',
            'supports' => array(
                'title',
            )
        )
	);
	register_post_type( 'eexams_subjects',
        array(
            'labels' => array(
                'name' => __( 'Subjects' ),
                'singular_name' => __( 'Subject' )
            ), 
            'public' => true,
            'has_archive' => true,
			'show_in_menu' => 'edit.php?post_type=eexams_questions',
            'supports' => array(
                'title',
            )
        )
    );
	register_post_type( 'eexams_diff',
        array(
            'labels' => array(
                'name' => __( 'Difficulty levels' ),
                'singular_name' => __( 'Difficulty level' )
            ), 
			'public' => true,
			'show_in_menu' => 'edit.php?post_type=eexams_questions',
            'has_archive' => true,
            'supports' => array(
                'title',
            )
        )
	);
	register_post_type( 'eexams_quizzes',
		array(
			'labels' => array(
				'name' => __( 'Quizzes' ),
				'singular_name' => __( 'Quizz' )
			), 
			'public' => true,
			'has_archive' => true,
			'show_in_menu' => 'edit.php?post_type=eexams_questions',
			'supports' => array(
				'title','custom-fields'
			)
		)
	);
	register_post_type( 'eexams_reports',
		array(
			'labels' => array(
				'name' => __( 'Reports' ),
				'singular_name' => __( 'Report' )
			), 
			'public' => true,
			'has_archive' => false,
			'show_in_menu' => 'edit.php?post_type=eexams_questions',
		)
	);
    	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Question Categories', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Question Category', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Question Categories', 'textdomain' ),
		'all_items'         => __( 'All Question Categories', 'textdomain' ),
		'parent_item'       => __( 'Parent Question Category', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Question Category:', 'textdomain' ),
		'edit_item'         => __( 'Edit Question Category', 'textdomain' ),
		'update_item'       => __( 'Update Question Category', 'textdomain' ),
		'add_new_item'      => __( 'Add New Question Category', 'textdomain' ),
		'new_item_name'     => __( 'New Question Category Name', 'textdomain' ),
		'menu_name'         => __( 'Question Category', 'textdomain' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'questions_category' ),
	);

	register_taxonomy( 'questions_category', array( 'eexams_questions' ), $args );

	// Add new taxonomy, NOT hierarchical (like tags)
	$labels = array(
		'name'                       => _x( 'Question Tags', 'taxonomy general name', 'textdomain' ),
		'singular_name'              => _x( 'Question Tag', 'taxonomy singular name', 'textdomain' ),
		'search_items'               => __( 'Search Question Tags', 'textdomain' ),
		'popular_items'              => __( 'Popular Question Tags', 'textdomain' ),
		'all_items'                  => __( 'All Question Tags', 'textdomain' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Question Tag', 'textdomain' ),
		'update_item'                => __( 'Update Question Tag', 'textdomain' ),
		'add_new_item'               => __( 'Add New Question Tag', 'textdomain' ),
		'new_item_name'              => __( 'New Question Tag Name', 'textdomain' ),
		'separate_items_with_commas' => __( 'Separate Question Tags with commas', 'textdomain' ),
		'add_or_remove_items'        => __( 'Add or remove Question Tags', 'textdomain' ),
		'choose_from_most_used'      => __( 'Choose from the most used Question Tags', 'textdomain' ),
		'not_found'                  => __( 'No Question Tags found.', 'textdomain' ),
		'menu_name'                  => __( 'Question Tags', 'textdomain' ),
	);

	$args = array(
		'hierarchical'          => false,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'questions_tags' ),
	);

	register_taxonomy( 'questions_tags', 'eexams_questions', $args );
}