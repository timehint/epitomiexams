window.onbeforeunload = null;
(function ($) {
  $('.yearselect').yearselect({
    start: 1950,
    end: (new Date()).getFullYear(),
  });
  var optionscount = 0;
  var type = EExams.type;
  switch (type) {
    case "radio":
      multipleAnswers();
      fillanswers();
      break;
    case "multi":
      multipleAnswers();
      fillanswers();
      break;
    case "pic":
      multipleAnswers();
      fillanswers();
      break;
    case "fill":
      multipleAnswers();
      fillanswers();
      break;
    case "freetext":
      singleAnswer();
      fillanswer();
      break;

    default:
      break;
  }

  function fillanswers() {
    var data = EExams.answers;
    data = JSON.parse(data);
    data.forEach(function (element, index, arrr) {
      if (type === "pic") {
        var htmltemp =
          '<input type="hidden" class="pic" value="' +
          element.pic +
          '" required id="img-' +
          optionscount +
          '">';
        htmltemp +=
          '<span class="dashicons dashicons-format-image upload-btn" data-input_id="#img-' +
          optionscount +
          '"></span>';
        htmltemp += "</label>";
        if (parseInt(EExams.correct_answer) === index) {
          htmltemp +=
            ' <input type="radio" value="' +
            optionscount +
            '" class="correct_answer" checked="checked">';
        } else {
          htmltemp +=
            ' <input type="radio" value="' +
            optionscount +
            '" class="correct_answer">';
        }
        $("#options").append(
          '<div class="option" id="op-' +
          optionscount +
          '"><label for="opt-' +
          optionscount +
          '">Option ' +
          (optionscount + 1) +
          ':</label><input type="text" value="' +
          element.value +
          '" class="ans" id="opt-' +
          optionscount +
          '" >' +
          htmltemp +
          '  <span style="cursor:pointer" data-id="' +
          optionscount +
          '" class="remove">X<span><br></div>'
        );
      } else {
        if (type === "radio") {
          if (parseInt(EExams.correct_answer) === index) {
            var htmltemp =
              ' <input type="radio" value="' +
              optionscount +
              '" class="correct_answer" checked="checked">';
          } else {
            var htmltemp =
              ' <input type="radio" value="' +
              optionscount +
              '" class="correct_answer">';
          }
          $("#options").append(
            '<div class="option" id="op-' +
            optionscount +
            '"><label for="opt-' +
            optionscount +
            '">Option ' +
            (optionscount + 1) +
            ':</label><input type="text" value="' +
            element +
            '" class="ans" id="opt-' +
            optionscount +
            '" > ' +
            htmltemp +
            ' <span style="cursor:pointer" data-id="' +
            optionscount +
            '" class="remove">X<span><br></div>'
          );
        } else if (type === "multi") {
          indexess = EExams.correct_answer.split("+").map(function (item) {
            return parseInt(item, 10);
          });
          if (indexess.includes(optionscount)) {
            var htmltemp =
              ' <input type="checkbox" value="' +
              optionscount +
              '" class="correct_answer" checked="checked">';
          } else {
            var htmltemp =
              ' <input type="checkbox" value="' +
              optionscount +
              '" class="correct_answer">';
          }
          $("#options").append(
            '<div class="option" id="op-' +
            optionscount +
            '"><label for="opt-' +
            optionscount +
            '">Option ' +
            (optionscount + 1) +
            ':</label><input type="text" value="' +
            element +
            '" class="ans" id="opt-' +
            optionscount +
            '" > ' +
            htmltemp +
            ' <span style="cursor:pointer" data-id="' +
            optionscount +
            '" class="remove">X<span><br></div>'
          );
        } else {
          $("#options").append(
            '<div class="option" id="op-' +
            optionscount +
            '"><label for="opt-' +
            optionscount +
            '">Option ' +
            (optionscount + 1) +
            ':</label><input type="text" value="' +
            element +
            '" class="ans" id="opt-' +
            optionscount +
            '" >  <span style="cursor:pointer" data-id="' +
            optionscount +
            '" class="remove">X<span><br></div>'
          );
        }
      }
      optionscount++;
    });
  }

  function fillanswer() {
    var data = EExams.answers;
    if (type == "freetext") {
      $("#answerROW").val(data);
    }
    return;
  }
  $("#question-type").on("change", function () {
    type = this.value;
    switch (this.value) {
      case "radio":
        multipleAnswers();
        break;
      case "multi":
        multipleAnswers();
        break;
      case "pic":
        multipleAnswers();
        break;
      case "fill":
        multipleAnswers();
        break;
      case "freetext":
        singleAnswer();
        break;

      default:
        break;
    }
    $("#type").val(type);
  });
  $("#answer").on("click", ".remove", function (param) {
    $("#op-" + $(this).data("id")).remove();
    optionscount--;
    $("#options")
      .children(".option")
      .each(function (indexInArray, valueOfElement) {
        $(this).attr("id", "op-" + (indexInArray - 1));
        $(this)
          .find("label")
          .attr("for", "opt-" + (indexInArray - 1));
        $(this)
          .find("label")
          .html("Option " + indexInArray + ":");
        $(this)
          .find("input")
          .attr("id", "opt-" + (indexInArray - 1));
      });
  });
  $("#answer").on("click", ".add-optbtn", function (param) {
    if (type === "pic") {
      var htmltemp =
        '<input type="hidden" class="pic" required id="img-' +
        optionscount +
        '">';
      htmltemp +=
        '<span class="dashicons dashicons-format-image upload-btn" data-input_id="#img-' +
        optionscount +
        '"></span>';
      htmltemp += "</label>";
      $("#options").append(
        '<div class="option" id="op-' +
        optionscount +
        '"><label for="opt-' +
        optionscount +
        '">Option ' +
        (optionscount + 1) +
        ':</label><input type="text" class="ans" id="opt-' +
        optionscount +
        '" >' +
        htmltemp +
        '  <input type="radio" value="' +
        optionscount +
        '" class="correct_answer"> <span style="cursor:pointer" data-id="' +
        optionscount +
        '" class="remove">X<span><br></div>'
      );
    } else {
      if (type === "radio") {
        $("#options").append(
          '<div class="option" id="op-' +
          optionscount +
          '"><label for="opt-' +
          optionscount +
          '">Option ' +
          (optionscount + 1) +
          ':</label><input type="text" class="ans" id="opt-' +
          optionscount +
          '" > <input type="radio" value="' +
          optionscount +
          '" class="correct_answer">  <span style="cursor:pointer" data-id="' +
          optionscount +
          '" class="remove">X<span><br></div>'
        );
      } else if (type === "multi") {
        $("#options").append(
          '<div class="option" id="op-' +
          optionscount +
          '"><label for="opt-' +
          optionscount +
          '">Option ' +
          (optionscount + 1) +
          ':</label><input type="text" class="ans" id="opt-' +
          optionscount +
          '" > <input type="checkbox" value="' +
          optionscount +
          '" class="correct_answer">  <span style="cursor:pointer" data-id="' +
          optionscount +
          '" class="remove">X<span><br></div>'
        );
      } else {
        $("#options").append(
          '<div class="option" id="op-' +
          optionscount +
          '"><label for="opt-' +
          optionscount +
          '">Option ' +
          (optionscount + 1) +
          ':</label><input type="text" class="ans" id="opt-' +
          optionscount +
          '" > <span style="cursor:pointer" data-id="' +
          optionscount +
          '" class="remove">X<span><br></div>'
        );
      }
    }
    optionscount++;
  });

  function multipleAnswers() {
    optionscount = 0;
    $("#answer").empty();
    var contain = $(
      "<div id='options' style='position: relative; padding:15px'><span class='add-optbtn button' style='position: absolute;top: -15px;right: 0;'>Add option<span></div>"
    );
    $("#answer").append(contain);
  }

  function singleAnswer() {
    optionscount = 0;
    $("#answer").empty();
    var contain = $(
      '<div id="Answer" style="position: relative; padding:15px"><label for="answerROW">rows:</label><input type="text" id="answerROW" ></div>'
    );
    $("#answer").append(contain);
  }
  $("#post").submit(function (e) {
    var obj;
    if (type === "freetext") {
      obj = $("#answerROW").val();
    } else {
      obj = [];
      $("#options")
        .children(".option")
        .each(function (index) {
          var temp = null;
          if (type === "pic") {
            temp = {
              value: $(this)
                .find("input.ans")
                .val(),
              pic: $(this)
                .find("input.pic")
                .val()
            };
          } else {
            temp = $(this)
              .find("input.ans")
              .val();
          }
          obj.push(temp);
        });
      obj = JSON.stringify(obj);
      if (type === "radio") {
        $("#correct_answer").val(
          $("input.correct_answer:checked").val()
        );
      } else if (type === "pic") {
        $("#correct_answer").val(
          $("input.correct_answer:checked").val()
        );
      } else if (type === "multi") {
        $('input.correct_answer:checked').each(function () {
          $("#correct_answer").val(
            this.value + "+" + $("#correct_answer").val()
          );
        });
      }
    }
    $("#answers").val(obj);
    $(this)
      .unbind("submit")
      .submit(); // continue the submit unbind preventDefault*/
  });
})(jQuery);
/*

media stuff

*/

// Uploading files
var file_frame;
var wp_media_post_id = wp.media.model.settings.post.id; // Store the old id
var set_to_post_id = 0; // Set this
jQuery(".upload-btn").live("click", function (event) {
  event.preventDefault();
  // If the media frame already exists, reopen it.
  if (file_frame) {
    // Set the post ID to what we want
    file_frame.uploader.uploader.param("post_id", set_to_post_id);
    // Open frame
    file_frame.open();
    return;
  } else {
    // Set the wp.media post id so the uploader grabs the ID we want when initialised
    wp.media.model.settings.post.id = set_to_post_id;
  }
  // Create the media frame.
  file_frame = wp.media.frames.file_frame = wp.media({
    title: jQuery(this).data("uploader_title"),
    button: {
      text: jQuery(this).data("uploader_button_text")
    },
    multiple: false, // Set to true to allow multiple files to be selected
    library: {
      type: ["image"]
    }
  });
  var input_id = jQuery(this).data("input_id");
  // When an image is selected, run a callback.
  file_frame.on("select", function () {
    // We set multiple to false so only get one image from the uploader
    attachment = file_frame
      .state()
      .get("selection")
      .first()
      .toJSON();
    // Do something with attachment.id and/or attachment.url here
    jQuery(input_id).val(attachment.url);
    // Restore the main post ID
    wp.media.model.settings.post.id = wp_media_post_id;
    file_frame = null;
  });
  // Finally, open the modal
  file_frame.open();
}); // Restore the main ID when the add media button is pressed
jQuery("a.add_media").on("click", function () {
  wp.media.model.settings.post.id = wp_media_post_id;
});
