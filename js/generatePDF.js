function IDGenerator() {

  this.length = 8;
  this.timestamp = +new Date;

  var _getRandomInt = function (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  this.generate = function () {
    var ts = this.timestamp.toString();
    var parts = ts.split("").reverse();
    var id = "";

    for (var i = 0; i < this.length; ++i) {
      var index = _getRandomInt(0, parts.length - 1);
      id += parts[index];
    }

    return id;
  }


}

window.onload = function () {
    
  function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
      r: parseInt(result[1], 16),
      g: parseInt(result[2], 16),
      b: parseInt(result[3], 16)
    } : null;
  }
  

  var mj2img = function (texstring) {
    return new Promise((resolve, reject) => {
      var input = texstring;
      var wrapper = document.getElementById("wrapper");
      wrapper.innerHTML = input;
      var output = {
        svg: "",
        img: ""
      };
      MathJax.Hub.Queue(["Typeset", MathJax.Hub, wrapper]);
      MathJax.Hub.Queue(function () {
        var mjOut = wrapper.getElementsByTagName("svg")[0];
        if (mjOut !== undefined) {
          mjOut.setAttribute("xmlns", "http://www.w3.org/2000/svg");
          // thanks, https://spin.atomicobject.com/2014/01/21/convert-svg-to-png/
          output.svg = mjOut.outerHTML;
          var image = new Image();
          image.src = "data:image/svg+xml;base64," + window.btoa(unescape(encodeURIComponent(output.svg)));
          image.onload = function () {
            var canvas = document.createElement("canvas");
            canvas.width = image.width;
            canvas.height = image.height;
            var context = canvas.getContext("2d");
            context.drawImage(image, 0, 0);
            output.img = canvas.toDataURL("image/PNG");
            var newEl = document.createElement("img");
            newEl.setAttribute("src", output.img);
            jQuery(".MathJax_Preview").remove();
            jQuery("#wrapper").find("script").remove();
            document.getElementsByClassName("MathJax_SVG_Display")[0].parentNode.replaceChild(newEl, document.getElementsByClassName("MathJax_SVG_Display")[0]);
            output.svg = wrapper.innerHTML;
            wrapper.innerHTML = "";
            resolve(output.svg);
          };
        } else {
          resolve(wrapper.innerHTML);
          wrapper.innerHTML = "";
        }
      });
    });
  };

  var body = jQuery(".body-wrapper");
  jQuery("body").empty().append(body);

  var margin = 10;
  let urlParams = new URLSearchParams(window.location.search);
  let title = urlParams.get("title");
  let subtitle = urlParams.get("subtitle");
  let answers = urlParams.get("answers");
  var line = margin;
  var DPI = parseInt(urlParams.get("pdfq"));  
  if(isNaN(DPI))
  {
    DPI = 5;
  }
  console.log(DPI);

  var questions = JSON.parse(EExams.questions);
  var doc = new jsPDF();
  doc.setFont("Roboto","normal");

  if(EExams.settings.EExams_pdf_title_enabled === "yes")
  {
    doc.setFontSize(parseInt(EExams.settings.EExams_pdf_title_font_size));
    var title_color = hexToRgb(EExams.settings.EExams_pdf_title_color);
    doc.setTextColor(title_color.r, title_color.g,title_color.b);

    doc.text(title, margin, line + 10);
    line += 12.5;
  }

  if(EExams.settings.EExams_pdf_subtitle_enabled === "yes")
  {
    doc.setFontSize(parseInt(EExams.settings.EExams_pdf_subtitle_font_size));
    var subtitle_title_color = hexToRgb(EExams.settings.EExams_pdf_subtitle_color);
    doc.setTextColor(subtitle_title_color.r, subtitle_title_color.g,subtitle_title_color.b);

    doc.text(subtitle, margin+2, line + 5);
    line += 12.5;
  
  }

  if(EExams.settings.EExams_pdf_box_grade_enabled === "yes")
  {
    doc.rect(doc.internal.pageSize.width - margin - 35, 2, 30, 25)
  }
  if(EExams.settings.EExams_pdf_name_enabled === "yes")
  {
    doc.setFontSize(parseInt(EExams.settings.EExams_pdf_name_font_size));
    var name_color = hexToRgb(EExams.settings.EExams_pdf_name_color);
    doc.setTextColor(name_color.r, name_color.g,name_color.b);
    
    doc.text(EExams.settings.EExams_pdf_name_text, doc.internal.pageSize.width - margin - 32, 35);
    doc.line(doc.internal.pageSize.width - margin - 40, 42,doc.internal.pageSize.width - margin, 42 )
    //doc.text("------------------------", doc.internal.pageSize.width - margin - 40, 42);
    line = 45;
  }
  


  var width = doc.internal.pageSize.width;    
  var height = doc.internal.pageSize.height;
  async function processquestion(question) {
    var generator = new IDGenerator();
    var wrap = generator.generate();
    document.getElementById("scrapHtml").innerHTML += "<div id='" + wrap + "'></di>"
    await mj2img(question.question).then(function (output) {
      
      if(EExams.settings.EExams_pdf_question_title_enabled === "yes")
      {
        document.getElementById(wrap).innerHTML += "<strong style='font-size:"+EExams.settings.EExams_pdf_question_title_font_size+"px; color:"+EExams.settings.EExams_pdf_question_title_color+"'>"+question.post.post_title+"</strong>";
      }

      if(EExams.settings.EExams_pdf_points_enabled === "yes")
      {
        document.getElementById(wrap).innerHTML += '<span class="points" style="position: absolute;right: calc(100% - 700px);font-size: '+EExams.settings.EExams_pdf_points_font_size+'px;color: '+EExams.settings.EExams_pdf_points_color+';">'+question.points+' '+EExams.settings.EExams_pdf_points_text+'</span>';
      }

      console.log(output.substring(0,3));
      if(output.substring(0,3) === "<p>")
      {
        document.getElementById(wrap).innerHTML +=output;
      }
      else
      {
        document.getElementById(wrap).innerHTML += "<p>" + output +"</p>";
      }
      jQuery("#" + wrap).find("p").css("font-size", "16px"); 
      switch (question.type) {
        case "radio":
          var generator = new IDGenerator();
          var id = generator.generate();
          jQuery("#" + wrap).append("<ul id='" + id + "' style='list-style:none'></ul>");
          JSON.parse(question.answers).forEach(function (element, index) {
            if (answers == "1") {
              if (index == parseInt(question.correct_answer)) {
                jQuery("#" + wrap).find("#" + id).append("<li><img style='height:20px;' src='" + EExams.iconURL + "RadioOn.png'> " + element + "</li>");
              } else {
                jQuery("#" + wrap).find("#" + id).append("<li><img style='height:20px;' src='" + EExams.iconURL + "RadioOff.png'> " + element + "</li>");
              }
            } else {
              jQuery("#" + wrap).find("#" + id).append("<li><img style='height:20px;' src='" + EExams.iconURL + "RadioOff.png'> " + element + "</li>");
            }
          });

          break;
        case "multi":
          var generator = new IDGenerator();
          var id = generator.generate();
          jQuery("#" + wrap).append("<ul id='" + id + "' style='list-style:none'></ul>");
          JSON.parse(question.answers).forEach(function (element, index) {
            if (answers == "1") {
              var Cans = question.correct_answer.split("+");
              var bool = false;
              for (let ans of Cans) {
                if (index == parseInt(ans)) {
                  bool = true;
                  break;
                } else {
                  bool = false;
                  break;
                }
              };
              if (bool) {
                jQuery("#" + wrap).find("#" + id).append("<li><img style='height:20px;' src='" + EExams.iconURL + "CheckOn.png'> " + element + "</li>");
              } else {
                jQuery("#" + wrap).find("#" + id).append("<li><img style='height:20px;' src='" + EExams.iconURL + "CheckOff.png'> " + element + "</li>");
              }
            } else {
              jQuery("#" + wrap).find("#" + id).append("<li><img style='height:20px;' src='" + EExams.iconURL + "CheckOff.png'> " + element + "</li>");
            }
          });
          break;
        case "pic":
          var single = true;
          JSON.parse(question.answers).forEach(function (element, index) {
            if (answers == "1") {
              if (index == parseInt(question.correct_answer)) {
                jQuery("#" + wrap).append("<div style='width: 46%;display: inline-block;margin: 2%;'><img style='height:150px;width:100%' src='" + element.pic + "'> <br><img style='height:20px;' src='" + EExams.iconURL + "RadioOn.png'> " + element.value + "</div>");
              } else {
                jQuery("#" + wrap).append("<div style='width: 46%;display: inline-block;margin: 2%;'><img style='height:150px;width:100%' src='" + element.pic + "'> <br><img style='height:20px;' src='" + EExams.iconURL + "RadioOff.png'> " + element.value + "</div>");
              }
            } else {
              jQuery("#" + wrap).append("<div style='width: 46%;display: inline-block;margin: 2%;'><img style='height:150px;width:100%' src='" + element.pic + "'> <br><img style='height:20px;' src='" + EExams.iconURL + "RadioOff.png'> " + element.value + "</div>");
            }
          });
          break;
        case "fill":
          if (answers == "1") {
            var answs = JSON.parse(question.answers);
            var ind = -1;
            document.getElementById(wrap).innerHTML = document.getElementById(wrap).innerHTML.replace(/\[s=(\d+)\]/g, function ($matched) {
              ind++;
              return answs[ind];
            });
          } else {
            document.getElementById(wrap).innerHTML = document.getElementById(wrap).innerHTML.replace(/\[s=(\d+)\]/g, "..........");
          }

          break;
        case "freetext":
          var html = "<p>";
          for (let index = 0; index < question.answers; index++) {
            html += "............................................................................................................................................................................<br>"
          }
          html += "</p>";
          document.getElementById(wrap).innerHTML += html
          break;

        default:
          break;
      }

      document.getElementById(wrap).innerHTML += "<div style='height: "+EExams.settings.EExams_pdf_questions_spacer+"px'></div>"
    });
    getSizeInCM = function (sizeInPX) {
      return (sizeInPX * 2.54) / (96 * window.devicePixelRatio);
    };
    return;
  }
  var current_page_number = 1;
  async function sanitizequestions() {
    document.getElementById("scrapHtml").innerHTML += "<br>";
    for (const question of questions) {
      await processquestion(question);
    }
    var children = document.getElementById("scrapHtml").children;
    for (var i = 0; i < children.length; i++) {
      var tableChild = children[i];
      if (jQuery("#scrapHtml2").height() + jQuery(tableChild).height() <= 1020) {
        jQuery(tableChild).clone().appendTo("#scrapHtml2");
      } else {

        var srcEl = document.querySelector("#scrapHtml2");
        srcEl.style.height = 1020 + "px"
        await html2canvas(srcEl, { scale: DPI }).then(
          async canvas => {
            var imgData = Canvas2Image.convertToJPEG(canvas);
            var dims = await getImageDimensions(imgData.src);
            await doc.addImage(imgData, "JPEG", margin, line,width-margin*2, (width-margin*2) * dims.h / dims.w);
          }
        );  
        jQuery("#scrapHtml2").html("");

        doc.line(margin, doc.internal.pageSize.height - 500, doc.internal.pageSize.width - margin, doc.internal.pageSize.height - 550);

        doc.addPage();
        current_page_number++;
        jQuery(tableChild).clone().appendTo("#scrapHtml2");
      }
    }
    var srcEl = document.querySelector("#scrapHtml2");
    srcEl.style.height = 1020 + "px"
    await html2canvas(srcEl, { scale: DPI }).then(
      async canvas => {
        var imgData = Canvas2Image.convertToJPEG(canvas);
        var dims = await getImageDimensions(imgData.src);
        await doc.addImage(imgData, "JPEG", margin, line,width-margin*2, (width-margin*2) * dims.h / dims.w);
      }
    );  
    jQuery("#scrapHtml2").html("");
    jQuery("#scrapHtml").html("");
    
    doc.line(margin, doc.internal.pageSize.height - 10, doc.internal.pageSize.width - margin, doc.internal.pageSize.height - 10);
    
    if(EExams.settings.EExams_pdf_footnote_enabled === "yes")
    {
      doc.setFontSize(parseInt(EExams.settings.EExams_pdf_footnote_font_size));
      var footnote_color = hexToRgb(EExams.settings.EExams_pdf_footnote_color);
      doc.setTextColor(footnote_color.r, footnote_color.g,footnote_color.b);
  
      doc.text(EExams.settings.EExams_pdf_footnote_text, margin, doc.internal.pageSize.height - 4);
    }

    if(EExams.settings.EExams_pdf_page_number_enabled === "yes")
    {
      doc.setFontSize(parseInt(EExams.settings.EExams_pdf_page_number_font_size));
      var pagenumber_color = hexToRgb(EExams.settings.EExams_pdf_page_number_color);
      doc.setTextColor(pagenumber_color.r, pagenumber_color.g,pagenumber_color.b);
  
      doc.text(current_page_number.toString(), doc.internal.pageSize.width - margin - 2, doc.internal.pageSize.height - 4);
    }

    var viewer = jQuery("#pdf-viewer");
    jQuery("body").empty().append(viewer);

    var data = URL.createObjectURL(doc.output("blob", title))
    console.log(data);
    PDFObject.embed(data, "#pdf-viewer");
  }

  function getImageDimensions(file) {
    return new Promise (function (resolved, rejected) {
      var i = new Image()
      i.onload = function(){
        resolved({w: i.width, h: i.height})
      };
      i.src = file
    })
  }

  sanitizequestions();

  function contrastImage(imgData, contrast){  //input range [-100..100]
    var d = imgData.data;
    contrast = (contrast/100) + 1;  //convert to decimal & shift range: [0..2]
    var intercept = 128 * (1 - contrast);
    for(var i=0;i<d.length;i+=4){   //r,g,b,a
        d[i] = d[i]*contrast + intercept;
        d[i+1] = d[i+1]*contrast + intercept;
        d[i+2] = d[i+2]*contrast + intercept;
    }
    return imgData;
  }

  function hiddenClone(element) {
    // Create clone of element
    var clone = element.cloneNode(true);

    // Position element relatively within the
    // body but still out of the viewport
    var style = clone.style;
    style.position = "relative";
    style.top = window.innerHeight + "px";
    style.left = 0;

    // Append clone to body and return the clone
    document.body.appendChild(clone);
    return clone;
  }
};