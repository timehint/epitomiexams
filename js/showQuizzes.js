(function ($) {
    $(".edit").on("click", function (event) {
        event.preventDefault()
        var id = $(this).attr("id");
        var titleID = $(this).data("title");
        swal("Enter A title:", {
            content: "input"
        }).then(value => {
            if(!!value.trim())
            {
                $.ajax({
                    type: "post",
                    url: EExams.ajaxurl,
                    data: {
                        id: id,
                        title: value,
                        action: "editQuizz"
                    },
                    dataType: "json",
                    success: function (response) { 
                        if (response.success == "true") {
                            $(titleID).html(value);
                        }
                    }
                });
            }
        });
    });
    $(".delete").on("click", function (event) {
        event.preventDefault()
        var id = $(this).attr("id");
        var row = $(this).data("row");
        $.ajax({
            type: "post",
            url: EExams.ajaxurl,
            data: {
                id: id,
                action: "deleteQuizz"
            },
            dataType: "json",
            success: function (response) {
                if (response.success == "true") {
                    $(row).remove();
                }
            }
        });
    });
    $(".export").on("click", function (event) {
        event.preventDefault()
        var id = $(this).attr("id");
        $.ajax({
            type: "post",
            url: EExams.ajaxurl,
            data: {
                id: id,
                action: "getQuizz"
            },
            dataType: "json",
            success: function (response) {
                if (response.success == "true") {
                    var url = EExams.exportUrl;
                    var ids = response.ids;
                    var answers = response.answers;
                    if (url.indexOf("?") > -1) {
                        url += "&answers=" + answers;
                    } else {
                        url += "?answers=" + answers;
                    }
                    url += "&pdfq="+EExams.pdfq+"&title=" + response.title + "&subtitle=" + response.subtitle + "&ids=" + ids;
                    $("<div class='modal'><iframe src='"+url+"'></iframe></div>").appendTo('body').modal();
                }
            }
        });
    });
})(jQuery);